/* Filename - rfscan.c
 * 
 * (c) Benoit Caillaud <benoit@caillaud.me>, December 2017
 *
 *  Overview: initializer RF generator and perform a frequency scan at a fixed level
 *
 *  This program has been tested on a Marconi 2019A RF generator 
 *
 *  This sample application is comprised of three basic parts:
 *
 *  1. Initialization
 *  2. Main Body
 *  3. Cleanup
 *
 *  The Initialization portion consists of getting a handle to a
 *  device and clearing the device.
 *
 *  In the Main Body, this application performs the following operations:
 *  1/ resets and initialize RF generator
 *  2/ set N=N1
 *  3/ while N<N2 do
 *  3.1/  wait tau
 *  3.2/  N=N+Delta
 *  3.3/  done
 *  4/ turn off RF output
 *
 *  The last step, Cleanup, takes the board offline.
 */

#ifdef GPIB_NI
#include "ni4882.h"
#endif

#ifdef GPIB_LINUX
#include "gpib/ib.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "mygpib.h"

#define BUFFSIZE 256

int main() {

   // Change primaryAddress to the address of the RF generator

  int device;
  int   primaryAddress = 5;      /* Primary address of the device           */
  int   secondaryAddress = 0;    /* Secondary address of the device         */
  char  rbuffer[BUFFSIZE];       /* Read buffer                             */
  char  wbuffer[BUFFSIZE];       /* Write buffer                            */
  unsigned int retVal;           /* Returned ibsta from NI-488.2 calls      */
  int len;

   // Frequency in kHz

   int frequency;

   // Frequency range and step
   
   int freq1 = 118000;
   int freq2 = 136000;
   int delta = 250;

   // Power level in dBm

   int level = -16;

   // Wait period in us

   useconds_t tau = 1000000;
   
/*****************************************************************************
 * Initialization - Done only once at the beginning of your application.
 *****************************************************************************/

   printf("Opening device %d:%d...",BOARD_INDEX,primaryAddress); fflush(stdout);

   device = ibdev(                /* Create a unit descriptor handle         */
         BOARD_INDEX,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
         primaryAddress,          /* Device primary address                  */
         secondaryAddress,        /* Device secondary address                */
         T3s,                     /* Timeout setting (T10s = 10 seconds)     */
         1,                       /* Assert EOI line at end of write         */
         0x180A);                 /* EOS termination mode                    */
   
   if (device == -1) {            /* Check for GPIB Error                    */
      gpibError("ibdev Error");
      return 2;
   }
 
   printf("Ok.\n");

   printf("Clear device..."); fflush(stdout);

   retVal = ibclr(device);        /* Clear the device                        */
   
   if (retVal & ERR) {
      gpibError("ibclr Error");
      return 2;
   }

   printf("Ok.\n");

/*****************************************************************************
 * Retrieve identity string
 *****************************************************************************/

//   printf("Identity string = "); fflush(stdout);

//   len = gpibCommand(device,"SF 11", rbuffer, BUFFSIZE);

//   if (len < 0)
//     {
//       printf("Error.\n");
//       return 2;
//     }

//   printf("%s\n",rbuffer);

/*****************************************************************************
 * Configure the RF generator
 *****************************************************************************/

   printf("Setting power level and frequency..."); fflush(stdout);

   sprintf(wbuffer,"C0, CF %d KZ, LV %d DB, C1",freq1,level);
   
   len = gpibCommand(device,wbuffer, (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

/*****************************************************************************
 * Scanning
 *****************************************************************************/

   for(frequency=freq1;frequency<=freq2;frequency+=delta)
     {
       printf("Setting F = %d kHz ...",frequency);

       sprintf(wbuffer,"CF %d KZ",frequency);
   
       len = gpibCommand(device,wbuffer, (char *)0, 0);

       if (len < 0)
	 {
	   printf("Error.\n");
	 }
       else
	 {
	   printf("Ok.\n");
	 }

       usleep((useconds_t)tau);
       
     }

/*****************************************************************************
 * Turning off the RF output
 *****************************************************************************/

   printf("Turning off the RF output...");

   len = gpibCommand(device,"C0", (char *)0, 0);

       if (len < 0)
	 {
	   printf("Error.\n");
	   return 2;
	 }

       printf("Ok.\n");
       
/*****************************************************************************
 * Cleanup - Done only once at the end of your application.
 *****************************************************************************/

   retVal = ibonl(device, 0);              /* Take the device offline                 */
   
   if (retVal & ERR) {
      gpibError("ibonl Error");
      return 2;
   }

   return 0;
}

