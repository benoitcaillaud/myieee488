#ifdef GPIB_NI
#include "ni4882.h"
#endif

#ifdef GPIB_LINUX
#include "gpib/ib.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mygpib.h"

/*
 *
 * Function gpibCommand
 *
 * Sends a null terminated string to the device.
 * If receive buffer pointer is not void and the
 * length is not zero, reads data from device.
 * Returns a negative value if an error occurred,
 * and the length of the data read if no error
 * occurred.
 *
 */

int gpibCommand(int device, char *cmd, char *rbuf, int blen)
{
  int clen = strlen(cmd);
  unsigned int ret;
  unsigned int err = 0;
  unsigned int rlen = 0;
  
  // Send command
  
  ret = ibwrt(device, cmd, clen);     /* Send the command   */
  
  if (ret & ERR) {
    gpibError("Error when sending command");
    return -1;
  }
  
  // return 0 if no read is necessary
  
  if ((rbuf == (char *)0) || (blen <= 0))
    {
      return 0;
    }
  
  // Read data

  ret = ibrd(device, rbuf, blen-1);

  if (ret & ERR) {
    gpibError("Error when reading data");
    return -1;
  }

#ifdef GPIB_NI  
  rlen = Ibcnt();
#endif

#ifdef GPIB_LINUX
  rlen = ibcnt;
#endif

  rbuf[rlen] = '\0';         /* Null terminate the ASCII string         */

  return rlen;
}

/*****************************************************************************
 *                      Function gpibError
 * This function will notify you that a NI-488 function failed by
 * printing an error message.  The status variable IBSTA will also be
 * printed in hexadecimal along with the mnemonic meaning of the bit
 * position. The status variable IBERR will be printed in decimal
 * along with the mnemonic meaning of the decimal value.  The status
 * variable IBCNTL will be printed in decimal.
 *
 * The NI-488 function IBONL is called to disable the hardware and
 * software.
 *
 * The EXIT function will terminate this program.
 *****************************************************************************/

void gpibError(char *msg) {

#ifdef GPIB_NI
  unsigned int status = Ibsta();
  unsigned int error = Iberr();
  unsigned int count = Ibcnt();
#endif

#ifdef GPIB_LINUX
  int status = ibsta;
  int error = iberr;
  int count = ibcnt;
#endif
  
  printf ("%s\n", msg);
    
  printf ("ibsta = &H%x  <", status);
  if (status & ERR )  printf (" ERR");
  if (status & TIMO)  printf (" TIMO");
  if (status & END )  printf (" END");
  if (status & SRQI)  printf (" SRQI");
  if (status & RQS )  printf (" RQS");
#ifdef GPIB_LINUX
  if (status & SPOLL) printf (" SPOLL");
  if (status & EVENT) printf (" EVENT");
#endif
  if (status & CMPL)  printf (" CMPL");
  if (status & LOK )  printf (" LOK");
  if (status & REM )  printf (" REM");
  if (status & CIC )  printf (" CIC");
  if (status & ATN )  printf (" ATN");
  if (status & TACS)  printf (" TACS");
  if (status & LACS)  printf (" LACS");
  if (status & DTAS)  printf (" DTAS");
  if (status & DCAS)  printf (" DCAS");
  printf (" >\n");
  
  printf ("iberr = %d", error);
  if (error == EDVR) printf (" EDVR <Driver error>\n");
  if (error == ECIC) printf (" ECIC <Not Controller-In-Charge>\n");
  if (error == ENOL) printf (" ENOL <No Listener>\n");
  if (error == EADR) printf (" EADR <Address error>\n");
  if (error == EARG) printf (" EARG <Invalid argument>\n");
  if (error == ESAC) printf (" ESAC <Not System Controller>\n");
  if (error == EABO) printf (" EABO <Operation aborted>\n");
  if (error == ENEB) printf (" ENEB <No GPIB board>\n");
  if (error == EDMA) printf (" EDMA <DMA error>\n");   
  if (error == EOIP) printf (" EOIP <Async I/O in progress>\n");
  if (error == ECAP) printf (" ECAP <No capability>\n");
  if (error == EFSO) printf (" EFSO <File system error>\n");
  if (error == EBUS) printf (" EBUS <Command error>\n");
  if (error == ESRQ) printf (" ESRQ <SRQ stuck on>\n");
  if (error == ETAB) printf (" ETAB <Table Overflow>\n");
#ifdef GPIB_NI
  if (error == ELCK) printf (" ELCK <Interface is locked>\n");
  if (error == EARM) printf (" EARM <ibnotify callback failed to rearm>\n");
  if (error == EHDL) printf (" EHDL <Input handle is invalid>\n");
  if (error == EWIP) printf (" EWIP <Wait in progress on specified input handle>\n");
  if (error == ERST) printf (" ERST <The event notification was cancelled due to a reset of the interface>\n");
  if (error == EPWR) printf (" EPWR <The interface lost power>\n");
#endif
  
  printf ("ibcnt = %u\n", count);
  printf ("\n");
  
  /* Call ibonl to take the device and interface offline */
  // ibonl (device, 0);
  
  exit(1);
}
