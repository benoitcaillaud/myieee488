/* Filename - scopedump.c
 * 
 *  Overview: download scope storage 
 *
 *  This sample application is comprised of three basic parts:
 *
 *  1. Initialization
 *  2. Main Body
 *  3. Cleanup
 *
 *  The Initialization portion consists of getting a handle to a
 *  device and clearing the device.
 *
 *  In the Main Body, this application send a DAT command and
 *  reads data.
 *
 *  The last step, Cleanup, takes the board offline.
 */

#ifdef GPIB_NI
#include "ni4882.h"
#endif

#ifdef GPIB_LINUX
#include "gpib/ib.h"
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "mygpib.h"

#define BUFFSIZE 20600
#define SMPSIZE 4096
#define STEP 0.002441406

int main() {
  int device;
  int   primaryAddress = 8;      /* Primary address of the device           */
  int   secondaryAddress = 0;    /* Secondary address of the device         */
  char  buffer[BUFFSIZE];             /* Read buffer                             */
  unsigned int retVal;           /* Returned ibsta from NI-488.2 calls      */
  int len;
  int n; /* number of samples */
  int i; /* sample index */
  int j; /* buffer index */
  int x;
  int y[SMPSIZE]; /* samples */
  char * filename = "scopedump_output.csv"; /* output file name */
  FILE * f;
  double timeBase = 1.0; /* horizontal time resolution (per division) */

/*****************************************************************************
 * Initialization - Done only once at the beginning of your application.
 *****************************************************************************/

   printf("Opening device %d:%d...",BOARD_INDEX,primaryAddress); fflush(stdout);

   device = ibdev(                /* Create a unit descriptor handle         */
         BOARD_INDEX,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
         primaryAddress,          /* Device primary address                  */
         secondaryAddress,        /* Device secondary address                */
         T3s,                     /* Timeout setting (T10s = 10 seconds)     */
         1,                       /* Assert EOI line at end of write         */
         0x180A);                 /* EOS termination mode                    */
   
   if (device == -1) {            /* Check for GPIB Error                    */
      gpibError("ibdev Error");
      return 2;
   }
 
   printf("Ok.\n");

   printf("Clear device..."); fflush(stdout);

   retVal = ibclr(device);        /* Clear the device                        */
   
   if (retVal & ERR) {
      gpibError("ibclr Error");
      return 2;
   }

   printf("Ok.\n");

   sleep(1);

/*****************************************************************************
 * Retrieve the device identity string, vertical and horizontal settings
 *****************************************************************************/
  printf("Retrieving device identity..."); fflush(stdout);

  len = gpibCommand(device, "IDT ?\n", buffer, BUFFSIZE);

  if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

  printf("Ok.\n");

  printf("Identity: %s\n", buffer);

  printf("Retrieving vertical settings..."); fflush(stdout);

  len = gpibCommand(device, "VER ?\n", buffer, BUFFSIZE);

  if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

  printf("Ok.\n");

  printf("Vertical settings: %s\n", buffer);

  printf("Retrieving horizontal settings..."); fflush(stdout);

  len = gpibCommand(device, "HOR ?\n", buffer, BUFFSIZE);

  if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

  printf("Ok.\n");

  printf("Horizontal settings: %s\n", buffer);

  printf("Retrieving horizontal resolution..."); fflush(stdout);

  len = gpibCommand(device, "HOR MTB, TIM ?\n", buffer, BUFFSIZE);

  if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

  printf("Ok.\n");

  if (sscanf(buffer,"TIM %lf",&timeBase)<1)
  {
    printf("Incorrect time resolution\n");
    return 3;
  }

  printf("Horizontal resolution: %e\n", timeBase);

/*****************************************************************************
 * Main Application Body - Write the majority of your GPIB code here.
 *****************************************************************************/
   printf("Reading data from instrument..."); fflush(stdout);

   len = gpibCommand(device,"REG 0, MSC TRACE, DATA_TYPE DECIMAL, DAT ?\n", buffer, BUFFSIZE);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   printf("Bytes read from instrument: %d\n", len); fflush(stdout);
	
/*****************************************************************************
 * Cleanup - Done only once at the end of your application.
 *****************************************************************************/

   retVal = ibonl(device, 0);              /* Take the device offline                 */
   
   if (retVal & ERR) {
      gpibError("ibonl Error");
      return 2;
   }

/*****************************************************************************
 * Parse the number of samples read
 *****************************************************************************/

  if ((len < 4) || (buffer[0]!='D') || (buffer[1]!='A') || (buffer[2]!='T') || (buffer[3]!=' '))
  {
    printf("Incorrect header\n");
    return 3;
  }

  if (sscanf(buffer+4,"%d",&n)<1)
  {
    printf("Incorrect sample count\n");
    return 3;
  }

  printf("sample count: %d\n",n);

  if (n > SMPSIZE)
  {
    printf("Too many sample: %d\n",n);
    return 3;
  }

/*****************************************************************************
 * Position on second line
 *****************************************************************************/
  printf("Parsing samples..."); fflush(stdout);


  j=0;
  while ((j < len) && (buffer[j]!='\n')) j++;

  j++;

/*****************************************************************************
 * Parse samples
 *****************************************************************************/

  for (i=0;((i<n) && (j<len));i++)
  {
    if (sscanf(buffer+j,"%d",&x)<1)
    {
      printf("Incorrect sample\n");
      return 3;
    }

    y[i] = x;

    while ((j < len) && (buffer[j]!='\n')) j++;
    j++;
  }

  printf("OK.\n");

/*****************************************************************************
 * Format data in CSV format
 *****************************************************************************/
  printf("Writing data to file %s...",filename); fflush(stdout);
  
  f = fopen(filename,"w");

  if (f == (FILE *)0)
    {
       printf("Failed to open file %s\n",filename);
       return 2;
    }

  fprintf(f,"t       ;u   \n");

  for (i=0;i<n;i++)
  {
    fprintf(f,"%8e;%4d\n",i*timeBase*STEP,y[i]);
  }

  if (fclose(f))
    {
      printf("Failed to close file %s\n",filename);
      return 2;
    }

  printf("Ok.\n");

  return 0;
}
