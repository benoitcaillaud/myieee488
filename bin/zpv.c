/* Filename - zpv.c
 * 
 * (c) Benoit Caillaud <benoit@caillaud.me>, December 2017
 *
 *  Overview: initializes a R&S ZPV vector analyzer 
 *
 *  This program has been tested on a R&S ZPV with E3 drawer 
 *
 *  This sample application is comprised of three basic parts:
 *
 *  1. Initialization
 *  2. Main Body
 *  3. Cleanup
 *
 *  The Initialization portion consists of getting a handle to a
 *  device and clearing the device.
 *
 *  In the Main Body, this application performs the following operations:
 *  1/ resets and initialize RF generator
 *  2/ sets frequnecy range and mode of operation
 *  3/ read A level
 *
 *  The last step, Cleanup, takes the board offline.
 */

#ifdef GPIB_NI
#include "ni4882.h"
#endif

#ifdef GPIB_LINUX
#include "gpib/ib.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "mygpib.h"

#define BUFFSIZE 256

int main() {

   // Change primaryAddress to the address of the RF generator

  int device;
  
  int   primaryAddress = 19;      /* Primary address of the device           */
  int   secondaryAddress = 0;    /* Secondary address of the device         */
  char  rbuffer[BUFFSIZE];       /* Read buffer                             */
  char  wbuffer[BUFFSIZE];       /* Write buffer                            */
  unsigned int retVal;           /* Returned ibsta from NI-488.2 calls      */
  int len;

  // Wait period in us

  useconds_t tau1 = 100000;
  useconds_t tau2 = 1100000;

  // Readings

  double level_a = 0.;
   
/*****************************************************************************
 * Initialization - Done only once at the beginning of your application.
 *****************************************************************************/

   printf("Opening device %d:%d...",BOARD_INDEX,primaryAddress); fflush(stdout);

   device = ibdev(                /* Create a unit descriptor handle         */
         BOARD_INDEX,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
         primaryAddress,          /* Device primary address                  */
         secondaryAddress,        /* Device secondary address                */
         T3s,                     /* Timeout setting (T10s = 10 seconds)     */
         1,                       /* Assert EOI line at end of write         */
         0x180A);                 /* EOS termination mode                    */
   
   if (device == -1) {            /* Check for GPIB Error                    */
      gpibError("ibdev Error");
      return 2;
   }
 
   printf("Ok.\n");

   printf("Clear device..."); fflush(stdout);

   retVal = ibclr(device);        /* Clear the device                        */
   
   if (retVal & ERR) {
      gpibError("ibclr Error");
      return 2;
   }

   printf("Ok.\n");

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Set frequency range
 *****************************************************************************/

   printf("Setting frequency range 0.6 to 1.0 GHz..."); fflush(stdout);

   len = gpibCommand(device,"Q1\nFR13\n", (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Set reading to A
 *****************************************************************************/

   printf("Setting reading to A..."); fflush(stdout);

   len = gpibCommand(device,"CA\n", (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Set log scale
 *****************************************************************************/

   printf("Setting log scale..."); fflush(stdout);

   len = gpibCommand(device,"DB\n", (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Set filter on
 *****************************************************************************/

   printf("Setting filter on..."); fflush(stdout);

   len = gpibCommand(device,"I1\n", (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Reading A level
 *****************************************************************************/

   usleep((useconds_t)tau2);
   
   printf("Reading A level..."); fflush(stdout);

   len = gpibCommand(device,"LX\n", rbuffer, 12);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   sscanf(rbuffer," %lf\n",&level_a);
   
   printf(" level A = %6.2lf dBm\n",level_a);

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Set reading to B/A
 *****************************************************************************/

   printf("Setting reading to B/A..."); fflush(stdout);

   len = gpibCommand(device,"BA\n", (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Set log scale
 *****************************************************************************/

   printf("Setting log scale..."); fflush(stdout);

   len = gpibCommand(device,"DB\n", (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Reading B/A level
 *****************************************************************************/

   usleep((useconds_t)tau2);
   
   printf("Reading B/A level..."); fflush(stdout);

   len = gpibCommand(device,"LX\n", rbuffer, 12);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   sscanf(rbuffer," %lf\n",&level_a);
   
   printf(" level B/A = %6.2lf dB\n",level_a);

   usleep((useconds_t)tau1);

/*****************************************************************************
 * Cleanup - Done only once at the end of your application.
 *****************************************************************************/

   printf("Closing..."); fflush(stdout);   
   
   retVal = ibonl(device, 0);              /* Take the device offline                 */
   
   if (retVal & ERR) {
      gpibError("ibonl Error");
      return 2;
   }

   printf("Ok.\n");
   
   return 0;
}

