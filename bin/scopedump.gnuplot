set datafile separator ";"
set autoscale fix
set key outside right center

set terminal pdf
set output "scopedump_output.pdf"

set title noenhanced


plot "scopedump_output.csv" using 1:2 title 'u' with lines
