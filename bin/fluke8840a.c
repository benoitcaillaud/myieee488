/* Filename - fluke8840a.c
 * 
 * (c) Benoit Caillaud <benoit@caillaud.eu>, December 2024
 *
 *  Overview: initializes a Fluke 8840 A multimeter, sets it
 *  in mA AC and then samples continuously with at a 1s period
 *  and outputs the measures in a .csv file
 *
 *  This program has been tested on a Fluke 8840 A multimeter with
 *  options IEEE-05 and AC-09. 
 *
 *  This sample application is comprised of three basic parts:
 *
 *  1. Initialization
 *  2. Main Body
 *  3. Cleanup
 *
 *  The Initialization portion consists of getting a handle to a
 *  device and clearing the device.
 *
 *  In the Main Body, this application performs the following operations:
 *  1/ reset, set in remote mode and initialize the multimeter
 *  2/ sets function, sample rate
 *  3/ loop n times: read data, write in a file
 *  4/ reset the multimeter, return it in local mode, close the device handle
 *
 */

#ifdef GPIB_NI
#include "ni4882.h"
#endif

#ifdef GPIB_LINUX
#include "gpib/ib.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef MS_WINDOWS
#include <unistd.h>
#endif

#include "mygpib.h"

#define BUFFSIZE 256
#define SAMPLES 100

int main() {

   // Change primaryAddress to the address of the RF generator

  int device;
  
  int   primaryAddress = 6;      /* Primary address of the device           */
  int   secondaryAddress = 0;    /* Secondary address of the device         */
  char  rbuffer[BUFFSIZE];       /* Read buffer                             */
  char  wbuffer[BUFFSIZE];       /* Write buffer                            */
  unsigned int retVal;           /* Returned ibsta from NI-488.2 calls      */
  int len;

  // Wait period in us

  #ifndef MS_WINDOWS
  useconds_t epsilon = 250000;
  useconds_t tau = 1000000;
  #endif

  // Readings

  double sample = 0.;
  int k;

  #ifndef MS_WINDOWS
  useconds_t time;
  #endif
   
/*****************************************************************************
 * Initialization - Done only once at the beginning of your application.
 *****************************************************************************/

   printf("Opening device %d:%d...",BOARD_INDEX,primaryAddress); fflush(stdout);

   device = ibdev(                /* Create a unit descriptor handle         */
         BOARD_INDEX,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
         primaryAddress,          /* Device primary address                  */
         secondaryAddress,        /* Device secondary address                */
         T3s,                     /* Timeout setting (T10s = 10 seconds)     */
         1,                       /* Assert EOI line at end of write         */
         0x180A);                 /* EOS termination mode                    */
   
   if (device == -1) {            /* Check for GPIB Error                    */
      gpibError("ibdev Error");
      return 2;
   }
 
   printf("Ok.\n");

   printf("Clear device..."); fflush(stdout);

   retVal = ibclr(device);        /* Clear the device                        */
   
   if (retVal & ERR) {
      gpibError("ibclr Error");
      return 2;
   }

   printf("Ok.\n");

   #ifndef MS_WINDOWS
   usleep((useconds_t)epsilon);
   #endif

/*****************************************************************************
 * Set function mA AC, auto range and slow conversion
 *****************************************************************************/

   printf("Setting to mA AC with autorange and slow sampling"); fflush(stdout);

   len = gpibCommand(device,"* X0 F6 R0 S0 T0", (char *)0, 0);

   if (len < 0)
     {
       printf("Error.\n");
       return 2;
     }

   printf("Ok.\n");

   #ifndef MS_WINDOWS
   usleep((useconds_t)epsilon);
   #endif

/*****************************************************************************
 * Loop
 *****************************************************************************/

   for(k=0;k<SAMPLES;k++)
   { 
   
    printf("Reading..."); fflush(stdout);

    len = gpibCommand(device,"?", rbuffer, BUFFSIZE);

    if (len < 0)
     {
        printf("Error.\n");
        return 2;
     }

   sscanf(rbuffer," %lf\n",&sample);
   
   printf(" i = %3.6lf V\n",sample); fflush(stdout);

   #ifndef MS_WINDOWS
   usleep((useconds_t)tau);
   #endif
  }

/*****************************************************************************
 * Cleanup - Done only once at the end of your application.
 *****************************************************************************/

   printf("Closing..."); fflush(stdout);   
   
   retVal = ibonl(device, 0);              /* Take the device offline                 */
   
   if (retVal & ERR) {
      gpibError("ibonl Error");
      return 2;
   }

   printf("Ok.\n");
   
   return 0;
}
