/* Filename - rfscan.c
 * 
 * (c) Benoit Caillaud <benoit@caillaud.me>, December 2017
 *
 *  Overview: initializer RF generator and perform a frequency scan at a fixed level
 *
 *  This program has been tested on a Marconi 2019A RF generator 
 *
 *  This sample application is comprised of three basic parts:
 *
 *  1. Initialization
 *  2. Main Body
 *  3. Cleanup
 *
 *  The Initialization portion consists of getting a handle to a
 *  device and clearing the device.
 *
 *  In the Main Body, this application performs the following operations:
 *  1/ resets and initialize RF generator
 *  2/ set N=N1
 *  3/ while N<N2 do
 *  3.1/  wait tau
 *  3.2/  N=N+Delta
 *  3.3/  done
 *  4/ turn off RF output
 *
 *  The last step, Cleanup, takes the board offline.
 */

#ifdef GPIB_NI
#include "ni4882.h"
#endif

#ifdef GPIB_LINUX
#include "gpib/ib.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "mygpib.h"

#define BUFFSIZE 256
#define TBLSIZE 1024

#define OUTNAME "output.csv"

// Usage function

void usage(char * c, char * m) {
  printf("Error when parsing command line: %s\n",m);
  printf("Usage: %s <options>\n",c);
  printf("Options:\n  -o <filename>\n  -s <stepsize>\n  -f <freq1>:<freq2>\n  -l <level>\n");
  exit(3);
}

// Main program

int main(int argc, char ** argv) {

  // table used to store measurements
  
  double tfreq[TBLSIZE];
  double tvswr[TBLSIZE];
  double tr[TBLSIZE];
  double tphi[TBLSIZE];
  
  // index in table
  
  int index = 0;

  // output file

  char * filename = OUTNAME;
  FILE * out;
  
   // Buffers and variables used to communicate with instruments
  
  char  rbuffer[BUFFSIZE];       /* Read buffer                             */
  char  wbuffer[BUFFSIZE];       /* Write buffer                            */
  unsigned int retVal;           /* Returned ibsta from NI-488.2 calls      */
  int len;

  // Change primaryAddressRf to the address of the RF generator
  
  int   primaryAddressRf = 5;      /* Primary address of the device           */
  int   secondaryAddressRf = 0;    /* Secondary address of the device         */
   
  // Change primaryAddressVa to the address of the vector analyzer
  
  int   primaryAddressVa = 19;      /* Primary address of the device           */
  int   secondaryAddressVa = 0;    /* Secondary address of the device         */
   
  // Device for the RF generator

  int deviceRf = 0;            /* Device unit descriptor                  */
   
  // Device for the vector analyzer

  int deviceVa = 0;            /* Device unit descriptor                  */
   
  // Frequency in kHz

  int frequency;

  // Frequency range and step
   
  int freq1 = 118000;
  int freq2 = 136000;
  int delta = 250;
  int band = -1;
  
  // Number of steps

  int step;
  
  // Power level in dBm
  
  int level = -24;

  // Wait period in us

  useconds_t tau1 = 1500000; // Stabilization of RF generator
  useconds_t tau2 = 500000; // Measurement period with filter out
  useconds_t tau3 = 2000000; // Measurement period with filter in

  // Readings

  double levelA = 0.;
  double levelR = 0.;
  double phi = 0.;
  
  // Auxiliary variables

  double p;
  double vswr;
  double f;
  int i;
  char sta;
  double u;

/*****************************************************************************
 * Parsing the command line
 *****************************************************************************/

  for (i=1;i<argc;i++) {
    if ((strlen(argv[i]) == 2) &&
	argv[i][0] == '-')
      {
	switch (argv[i][1]) {
	  
	case 'o':
	  i++;
	  if (i < argc) {
	    filename = argv[i];
	  } else {
	    usage(argv[0],"-o expects an argument");
	  }
	  break;

	case 's':
	  i++;
	  if (i < argc) {
	    sscanf(argv[i],"%d",&delta);
	  } else {
	    usage(argv[0],"-s expects an argument");
	  }
	  break;

	case 'f':
	  i++;
	  if (i < argc) {
	    sscanf(argv[i],"%d:%d",&freq1,&freq2);
	  } else {
	    usage(argv[0],"-f expects an argument");
	  }
	  break;

	case 'l':
	  i++;
	  if (i < argc) {
	    sscanf(argv[i],"%d",&level);
	  } else {
	    usage(argv[0],"-l expects an argument");
	  }
	  break;

	default:
	  usage(argv[0],"invalid argument");
	}
      } else {
      usage(argv[0],"invalid argument");
    }
  }
  
/*****************************************************************************
 * Checking parameters validity
 *****************************************************************************/

  if ((delta > 0) &&
      (freq1 >= 300) &&
      (freq2 >= freq1) &&
      (freq2 < 1040000))
    {
      step = (freq2 - freq1) / delta;
      if (step * delta == freq2 - freq1) {
	step++;
      }

      if (step > TBLSIZE)
	{
	  printf("Number of steps exceeds maximal step count\n");
	  step = TBLSIZE;
	}
    } else {
    printf("Incorrect frequency parameters\n");
    exit(3);
  }

  freq2 = freq1 + delta * (step-1);
  
  if ((level >= -99) && (level <= 6)) {
    u = sqrt( 50. * pow(10.,(((double)level) / 10.)));
  } else {
    printf("Incorrect power level\n");
    exit(3);
  }
  
  printf("Parameters: %d steps from %d kHz to %d kHz with step size of %d kHz\nPower level %d dBm (%e V)\n",
	 step, freq1, freq2, delta, level, u);

/*****************************************************************************
 * Determination of frequency band
 *****************************************************************************/

  if ((freq1 >= 300) && (freq2 <= 1000)) { band = 1; }
  if ((freq1 >= 1000) && (freq2 <= 2000)) { band = 2; }
  if ((freq1 >= 2000) && (freq2 <= 3000)) { band = 3; }
  if ((freq1 >= 3000) && (freq2 <= 6000)) { band = 4; }
  if ((freq1 >= 6000) && (freq2 <= 10000)) { band = 5; }
  if ((freq1 >= 10000) && (freq2 <= 20000)) { band = 6; }
  if ((freq1 >= 20000) && (freq2 <= 30000)) { band = 7; }
  if ((freq1 >= 30000) && (freq2 <= 60000)) { band = 8; }
  if ((freq1 >= 60000) && (freq2 <= 100000)) { band = 9; }
  if ((freq1 >= 100000) && (freq2 <= 200000)) { band = 10; }
  if ((freq1 >= 200000) && (freq2 <= 300000)) { band = 11; }
  if ((freq1 >= 300000) && (freq2 <= 600000)) { band = 12; }
  if ((freq1 >= 600000) && (freq2 <= 1000000)) { band = 13; }
  if ((freq1 >= 1000000) && (freq2 <= 2000000)) { band = 14; }

  if (band < 0) {
    printf("Error: frequency range spans over more than one band\n");
    exit(3);
  }

/*****************************************************************************
 * Initialization of the RF generator.
 *****************************************************************************/

  printf("Opening RF generator %d:%d...",BOARD_INDEX,primaryAddressRf); fflush(stdout);

  deviceRf =                      /* Create a unit descriptor handle         */
    ibdev(BOARD_INDEX,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
	  primaryAddressRf,          /* Device primary address                  */
	  secondaryAddressRf,        /* Device secondary address                */
	  T3s,                     /* Timeout setting (T10s = 10 seconds)     */
	  1,                       /* Assert EOI line at end of write         */
	  0x180A);                 /* EOS termination mode                    */
   
  if (deviceRf == -1) {            /* Check for GPIB Error                    */
    gpibError("ibdev Error");
    return 2;
  }
 
  printf("Ok.\n");

  printf("Clear RF generator..."); fflush(stdout);

  retVal = ibclr(deviceRf);        /* Clear the device                        */
   
  if (retVal & ERR) {
    gpibError("ibclr Error");
    return 2;
  }

  printf("Ok.\n");

/*****************************************************************************
 * Configure the RF generator
 *****************************************************************************/

  printf("Setting power level and frequency..."); fflush(stdout);

  sprintf(wbuffer,"C0, CF %d KZ, LV %d DB, C1",freq1,level);
   
  len = gpibCommand(deviceRf, wbuffer, (char *)0, 0);

  if (len < 0)
    {
      printf("Error.\n");
      return 2;
    }

  printf("Ok.\n");

/*****************************************************************************
 * Initialization of the vector analyzer.
 *****************************************************************************/

  printf("Opening vector analyzer %d:%d...",BOARD_INDEX,primaryAddressVa); fflush(stdout);

  deviceVa =                      /* Create a unit descriptor handle         */
    ibdev(BOARD_INDEX,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
	  primaryAddressVa,          /* Device primary address                  */
	  secondaryAddressVa,        /* Device secondary address                */
	  T3s,                     /* Timeout setting (T10s = 10 seconds)     */
	  1,                       /* Assert EOI line at end of write         */
	  0x180A);                 /* EOS termination mode                    */
   
  if (deviceVa == -1) {            /* Check for GPIB Error                    */
    gpibError("ibdev Error");
    return 2;
  }
 
  printf("Ok.\n");

  printf("Clear vector analyzer..."); fflush(stdout);

  retVal = ibclr(deviceVa);        /* Clear the device                        */
   
  if (retVal & ERR) {
    gpibError("ibclr Error");
    return 2;
  }

  printf("Ok.\n");

/*****************************************************************************
 * Set frequency range
 *****************************************************************************/

  printf("Setting frequency band to %d...",band); fflush(stdout);

  sprintf(wbuffer,"Q1\nFR%02d\n",band);
  
  len = gpibCommand(deviceVa, wbuffer, (char *)0, 0);
   
  if (len < 0)
    {
      printf("Error.\n");
      return 2;
    }

  printf("Ok.\n");

/*****************************************************************************
 * Scanning
 *****************************************************************************/

  for(frequency=freq1;frequency<=freq2;frequency+=delta)
    {
      // computing frequency in MHz

      f = ((double)frequency)/1000.;
      
      // Setting frequency
       
      printf("Setting F = %d kHz ...",frequency);
      
      sprintf(wbuffer,"CF %d KZ",frequency);
      
      len = gpibCommand(deviceRf,wbuffer, (char *)0, 0);

      if (len < 0)
	{
	  printf("Error.\n");
	}
      else
	{
	  printf("Ok.\n");
	}

      // Preparing vector analyzer for forward power measurement

      printf("Setting reading to A in log. scale..."); fflush(stdout);
      
      len = gpibCommand(deviceVa,"CA\nDB\nI0\n", (char *)0, 0);

      if (len < 0)
	{
	  printf("Error.\n");
	  return 2;
	}

      printf("Ok.\n");

      // Waiting for stabilization of the generator and at least one valid measurement
       
      usleep((useconds_t)tau1);

      // Polling vector analyzer until synchronized

      sta = 32;

      while (sta & 32) {

	usleep(tau2);
	
	printf("Polling vector analyzer..."); fflush(stdout);

	sta=0;
      
	ibrsp(deviceVa,&sta);

	printf("%d",sta);

	if (sta & 64) { printf(" SRQ"); }
	if (sta & 32) { printf(" NSY"); }
	if (sta & 16) { printf(" OVA"); }
	if (sta & 8) { printf(" OVB"); }
	if (sta & 4) { printf(" UNB"); }

	printf("\n");
      }

      if (sta) {
	printf("Vector analyzer error. status=%d\n",sta);
      }
      
      // Reading forward power
      
      printf("Reading A level..."); fflush(stdout);

      len = gpibCommand(deviceVa,"LX\n", rbuffer, 12);

      if (len < 0)
	{
	  printf("Error.\n");
	  return 2;
	}

      sscanf(rbuffer," %lf\n",&levelA);
   
      printf(" level A = %6.2lf dBm\n",levelA);

      if (levelA < -50.)
	{
	  printf("Warning: power level is too low for valid VSWR measurement.\n");
	}
       
       //

      printf("Setting reading to B/A in lin. scale..."); fflush(stdout);

      len = gpibCommand(deviceVa,"BA\nLI\nI1\n", (char *)0, 0);

      if (len < 0)
	{
	  printf("Error.\n");
	  return 2;
	}

      printf("Ok.\n");

      // Waiting for at least one valid measurement
       
      usleep((useconds_t)tau3);

      // Measuring reflected voltage ratio
       
      printf("Reading B/A level..."); fflush(stdout);

      len = gpibCommand(deviceVa,"LX\n", rbuffer, 12);

      if (len < 0)
	{
	  printf("Error.\n");
	  return 2;
	}

      sscanf(rbuffer," %lf\n",&levelR);
   
      printf(" level B/A = %6.4lf\n",levelR);

      // Measuring phase

      printf("Reading phase B wrt. A..."); fflush(stdout);

      len = gpibCommand(deviceVa,"RX\n", rbuffer, 12);

      if (len < 0)
	{
	  printf("Error.\n");
	  return 2;
	}

      sscanf(rbuffer," %lf\n",&phi);
   
      printf(" phase = %5.1lf deg\n",phi);

      // Compute VSWR

      p = levelR;
      
      vswr = (1. + p) / (1. - p);

      printf("VSWR @ %7.3lf MHz = %8.2lf\n",f,vswr);

      // store in table

      tfreq[index] = f;
      tvswr[index] = vswr;
      tr[index] = p;
      tphi[index] = phi;

      index++;
    }

/*****************************************************************************
 * Turning off the RF output
 *****************************************************************************/

  printf("Turning off the RF output..."); fflush(stdout);

  len = gpibCommand(deviceRf,"C0", (char *)0, 0);

  if (len < 0)
    {
      printf("Error.\n");
      return 2;
    }

  printf("Ok.\n");
       
/*****************************************************************************
 * Cleanup RF generator
 *****************************************************************************/

  printf("Releasing RF generator..."); fflush(stdout); 
       
  retVal = ibonl(deviceRf, 0);              /* Take the device offline                 */
  
  if (retVal & ERR) {
    gpibError("ibonl Error");
    return 2;
  } else {
    printf("Ok.\n");
  }
       
/*****************************************************************************
 * Cleanup vector analyzer
 *****************************************************************************/

  printf("Releasing vector analyzer..."); fflush(stdout); 
  
  retVal = ibonl(deviceVa, 0);              /* Take the device offline                 */
   
  if (retVal & ERR) {
    gpibError("ibonl Error");
    return 2;
  } else {
    printf("Ok.\n");
  }

/*****************************************************************************
 * Dump table
 *****************************************************************************/

  printf("Writing results to CSV file %s...",filename);
  
  if ((out = fopen(filename,"w")) == (FILE *)0) {
    printf("Failed.\n");
  } else {
    fprintf(out,"Frequency (MHz);VSWR;Gamma;Phi (deg)\n");
  
    for(i=0;i<index;i++) {
      fprintf(out,"%lf;%lf;%lf;%lf\n",tfreq[i],tvswr[i],tr[i],tphi[i]);
    }
  }

  fclose(out);

  printf("Ok.\n");
  
/*****************************************************************************
 * Done
 *****************************************************************************/
   
  return 0;
}
