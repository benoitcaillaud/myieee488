#ifndef __MYGPIB_H__
#define __MYGPIB_H__

// Interface Index (GPIB0=0,GPIB1=1,etc.)

#define BOARD_INDEX 0

extern void gpibError(char *msg); // Error function declaration
extern int gpibCommand(int dev, char *cmd, char *buf, int lbuf); // Send command and receive result

#endif

