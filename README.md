MyIeee488 is a set of utilities for controlling instruments using the
IEEE 488 bus. The following instruments are supported:

  * Rohde & Schwarz ZPV vector analyser
  * Marconi 1019A RF generator
  * Phillips digital oscilloscope
  * Fluke 8840A multimeter
  * Keithley 194 high-speed voltmeter

This software is in C, using the NI IEEE488 library. It has been
tested with a NI USB to IEEE 488 interface.

Copyright (c) Benoit Caillaud <benoit@caillaud.me>, 2015-2017, 2023

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
“Software”), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
